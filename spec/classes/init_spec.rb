require 'spec_helper'
describe 'ceph_puppet_packages' do
  context 'with default values for all parameters' do
    it { should contain_class('ceph_puppet_packages') }
  end
end
