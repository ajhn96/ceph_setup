require 'json'

ceph_disk_json = Facter::Core::Execution.exec('ceph-disk list /dev/sd* --format json')
ceph_disk_parsed = JSON.parse(ceph_disk_json, :symbolize_names => true)
Facter.add(:ceph_disk, :type => :aggregate) do
        chunk(:path) do
                disks = {}
#                ceph_disk_parsed = JSON.parse(ceph_disk_json, :symbolize_names => true)
                ceph_disk_parsed.each do |disk|
                        partitions = {}
#                        disks[disk[:path]] = {:osd => false}
                        if disk[:partitions] then
                                disk[:partitions].each do |partition|
                                        if partition[:type] == 'data' then
                                                disks[disk[:path]] = {:osd => true}
                                        end
                                        partitions[partition[:path]] = partition
                                end
                        end
                        #disks[disk[:path]] = {:info => disk}
                        disks[disk[:path]] = {:partitions => partitions}             
                end
                disks
        end
        chunk(:info) do
                disks = {}
                ceph_disk_parsed.each do |disk|
                        if disk[:partitions] then
                                disks[disk[:path]] = {:info => {:path => disk[:path]}}
                        else
                                disks[disk[:path]] = {:info => disk}
                        end
                end
                disks
        end
        chunk(:osd) do
                disks = {}
                ceph_disk_parsed.each do |disk|
                        disks[disk[:path]] = {:osd => false}
                        if disk[:partitions] then
                                disk[:partitions].each do |partition|
                                        if partition[:type] == 'data' then
                                                disks[disk[:path]] = {:osd => true}
                                        end
                                end
                        end
                end
                disks
        end
end
