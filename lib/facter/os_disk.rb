Facter.add('os_disk') do
        setcode do
                Facter::Core::Execution.exec('df / --output=source | grep -o \/dev\/sd.')
        end
end
