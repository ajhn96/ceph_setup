#
class ceph_setup::pkgs {
  package { 'xfsprogs': ensure => 'latest' }
  package { 'vim':      ensure => 'latest' }
  package { 'git':      ensure => 'latest' }
  package { 'htop':      ensure => 'latest' }
}                
