# Class: cephi_setup
# ===========================
#
# Full description of class ceph_puppet_packages here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `sample parameter`
# Explanation of what this parameter affects and what it defaults to.
# e.g. "Specify one or more upstream ntp servers as an array."
#
# Variables
# ----------
#
# Here you should define a list of variables that this module would require.
#
# * `sample variable`
#  Explanation of how this variable affects the function of this class and if
#  it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#  External Node Classifier as a comma separated list of hostnames." (Note,
#  global variables should be avoided in favor of class parameters as
#  of Puppet 2.6.)
#
# Examples
# --------
#
# @example
#    class { 'ceph_setup':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    }
#
# Authors
# -------
#
# Author Name <ajhn96@mst.edu>
#
# Copyright
# ---------
#
# Copyright 2017 Alex Hoeft, unless otherwise noted.
#
class ceph_setup {
  $facts['ceph_disk'].each |$dsk| {
    if $dsk[0] != $facts['os_disk'] {
      if !$dsk[1]['osd'] {
        $dsk_path = $dsk[0]
        exec { "/usr/sbin/ceph-disk zap $dsk_path":}
        ceph::osd{
          $dsk[0]:
        }
      }
    }
  }
}
